# Calculadora de IMC
Una calculadora de Índice de Masa Corporal, escrita en Java.

## Obtener el código
Puedes clonar el repositorio, y abrir el proyecto con NetBeans.
Requiere Java SE 8 (no se ha probado con otras API de Java).

## Uso, modificación y redistribución
Este software se distribuye bajo la licencia GPL v3. Puedes leerla
en el siguiente enlace: https://www.gnu.org/licenses/gpl-3.0.en.html
