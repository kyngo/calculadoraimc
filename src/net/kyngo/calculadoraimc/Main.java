/*
 * Calculadora de IMC (Índice de Masa Corporal)
 * Creado por Arnau 'Kyngo' Martín.
 */
package net.kyngo.calculadoraimc;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // Declaración de variables
        float masa = 0;
        float altura = 0;
        // Objeto Scanner
        Scanner input = new Scanner(System.in);
        
        // Preguntará el peso hasta que se ponga algo que no valga 0
        do {
            System.out.println("Introduce tu peso, en kilos:");
            masa = input.nextFloat();
        } while (masa == 0);
        
        // Preguntará la altura hasta que se ponga algo que no valga 0
        do {
            System.out.println("Introduce tu altura, en centímetros:");
            altura = input.nextFloat();
        } while (altura == 0);
        
        // Cálculo de IMC ( IMC = Masa/Altura^2 )
        float powAltura = (float)Math.pow(altura, 2);
        float imc = (masa / powAltura) * 10000;
        
        // Muestra el resultado del cálculo y algún consejo
        System.out.printf("Tu IMC es de %.2f\n", imc);
        if (imc < 16) {
            System.out.println("Tu peso es demasiado bajo, ve a un médico a que te ayude a subir.");
        } else if (imc >= 16 && imc < 18.49) {
            System.out.println("Tu peso es bajo. Deberías considerar ganar algo de peso.");
        } else if (imc >= 18.5 && imc < 24.99) {
            System.out.println("Tienes un peso normal.");
        } else if (imc >= 25 && imc < 29.99) {
            System.out.println("Tienes sobrepeso. Deberías considerar bajar algo tu peso.");
        } else if (imc >= 30 && imc < 39.99) {
            System.out.println("Tienes obesidad. Deberías consultar a un dietista y cambiar tus hábitos.");
        } else {
            System.out.println("Tienes obesidad mórbida. Visita a tu médico para que te ayude lo antes posible.");
        }
    }
}
